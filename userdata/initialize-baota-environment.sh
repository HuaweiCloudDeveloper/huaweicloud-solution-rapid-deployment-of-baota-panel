#! /bin/bash

set -e

command -v wget > /dev/null 2>&1 || { yum install -y wget; }

wget -O /root/install.sh http://download.bt.cn/install/install_6.0.sh && yum install -y expect

[[ -f /root/install.sh ]] && expect <<-EOF
    set timeout -1

    spawn sh /root/install.sh ed8484bec

    expect {
        "(y/n):" { send y\n }
    }

    expect eof
EOF

rm -f /root/install.sh
