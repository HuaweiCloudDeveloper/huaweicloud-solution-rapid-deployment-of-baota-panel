[TOC]

**解决方案介绍**
===============
该解决方案能帮您快速使用云耀云服务器 HECS部署[宝塔](https://www.bt.cn/new/index.html)面板，为您提供永久免费、安全高效的服务器运维体验。适用于需要使用宝塔面板轻松管理服务器的运维场景。

解决方案实践详情页面地址：[https://www.huaweicloud.com/solution/implementations/rapid-deployment-of-baota-panel.html](https://www.huaweicloud.com/solution/implementations/rapid-deployment-of-baota-panel.html)

**架构图**
---------------
![方案架构](./document/rapid-deployment-of-baota-panel.png)

**架构描述**
---------------
该解决方案会部署如下资源：

1、创建1台云耀云服务器 HECS，安装宝塔面板。

2、创建1个弹性公网IP EIP，并绑定云耀云服务器 HECS，用于访问宝塔面板以及远程连接使用。

3、创建安全组，通过配置安全组规则，为云耀云服务器提供安全防护

**组织结构**
---------------

``` lua
huaweicloud-solution-rapid-deployment-of-baota-panel
├── rapid-deployment-of-baota-panel.tf.json -- 资源编排模板
├── userdata
	├── initialize-baota-environment.sh -- 脚本配置文件

```
**开始使用**
---------------
**安全组规则修改（可选）**

> [**须知**]
>
> - 该解决方案使用8888端口用来访问宝塔面板，默认对该方案创建的VPC子网网段放开，请参考[修改安全组规则](https://www.huaweicloud.com/solution/implementations/rapid-deployment-of-baota-panel.html)，配置IP地址白名单，以便能正常访问服务。
> - 该解决方案使用22端口用来远程登录云耀云服务器HECS，默认对该方案创建的VPC子网网段放开，请参考[修改安全组规则](https://www.huaweicloud.com/solution/implementations/rapid-deployment-of-baota-panel.html)，配置IP地址白名单，以便能正常访问服务。

安全组实际是网络流量访问策略，包括网络流量入方向规则和出方向规则，通过这些规则为安全组内具有相同保护需求并且相互信任的云服务器、云容器等实例提供安全保护。如果您的实例关联的安全组策略无法满足使用需求，比如需要添加、修改、删除某个TCP端口，请参考以下内容进行修改。

- 添加安全组规则：根据业务使用需求需要新开放某个TCP端口，请参考[添加安全组规则](https://support.huaweicloud.com/usermanual-vpc/zh-cn_topic_0030969470.html)添加入方向规则，打开指定的TCP端口。

- 修改安全组规则：安全组规则设置不当会造成严重的安全隐患。您可以参考[修改安全组规则](https://support.huaweicloud.com/usermanual-vpc/vpc_SecurityGroup_0005.html)，来修改安全组中不合理的规则，保证云服务器等实例的网络安全。

- 删除安全组规则：当安全组规则入方向、出方向源地址/目的地址有变化时，或者不需要开放某个端口时，您可以参考[删除安全组](https://support.huaweicloud.com/usermanual-vpc/vpc_SecurityGroup_0006.html)规则进行安全组规则删除。。

1.登录[华为云控制台](https://console.huaweicloud.com/console/?region=cn-north-4#/home)。

图1 华为云控制台

![华为云控制台](./document/readme-image-001.png)

2.在[云耀云服务器HECS控制台](https://console.huaweicloud.com/lcs/?agencyId=21c19ac150bf4867a8302133acfa94ec&region=cn-north-4&locale=zh-cn#/lcs/manager/vmList)，查看该方案一键部署创建的HECS实例及其绑定的弹性公网IP。

图2 HECS实例

![HECS实例](./document/readme-image-002.png)

3.通过MobaXterm或其他SSH客户端以账号密码方式连接云服务器即可登录。SSH默认的连接端口为22。

图3 登录HECS

![登录HECS](./document/readme-image-003.png)

> [**温馨提示**]
>
> - 查看访问宝塔面板需要的账号密码等登录信息需要执行/etc/init.d/bt default命令进行查看。

4.在浏览器地址栏中输入记录的“Bt-Panel”参数后面的地址，例如：http://弹性公网IP:8888/46722528。在宝塔面板的登录页面中，输入记录的账户和密码即可完成登录。

图4 登录宝塔面板

![登录宝塔面板](./document/readme-image-004.png)